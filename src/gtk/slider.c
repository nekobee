/* nekobee DSSI software synthesizer GUI
 *
 * Most of this code comes from gAlan 0.2.0, copyright (C) 1999
 * Tony Garnock-Jones, with modifications by Sean Bolton,
 * copyright (c) 2004.  (gtkdial.c rolls over in its grave.)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program; if not, write to the Free
 * Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307, USA.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <math.h>
#include <stdio.h>
#include <gtk/gtkmain.h>
#include <gtk/gtksignal.h>

#include "slider.h"

#ifndef M_PI
# define M_PI		3.14159265358979323846	/* pi */
#endif

#define SCROLL_DELAY_LENGTH	300

// eventually I'll make this a more "general" control
// for now, it's just a slide switch ;-)
#define SLIDER_SIZE		50

#define STATE_IDLE		0
#define STATE_PRESSED		1
#define STATE_DRAGGING		2

static void gtk_slider_class_init(GtksliderClass *klass);
static void gtk_slider_init(Gtkslider *slider);
static void gtk_slider_destroy(GtkObject *object);
static void gtk_slider_realize(GtkWidget *widget);
static void gtk_slider_size_request(GtkWidget *widget, GtkRequisition *requisition);
static void gtk_slider_size_allocate(GtkWidget *widget, GtkAllocation *allocation);
static gint gtk_slider_expose(GtkWidget *widget, GdkEventExpose *event);
static gint gtk_slider_button_press(GtkWidget *widget, GdkEventButton *event);
static gint gtk_slider_button_release(GtkWidget *widget, GdkEventButton *event);
static gint gtk_slider_motion_notify(GtkWidget *widget, GdkEventMotion *event);
static gint gtk_slider_timer(Gtkslider *slider);

static void gtk_slider_update_mouse_update(Gtkslider *slider);
static void gtk_slider_update_mouse(Gtkslider *slider, gint x, gint y, gboolean absolute);
static void gtk_slider_update(Gtkslider *slider);
static void gtk_slider_adjustment_changed(GtkAdjustment *adjustment, gpointer data);
static void gtk_slider_adjustment_value_changed(GtkAdjustment *adjustment, gpointer data);

GError *gerror;

/* Local data */

static GtkWidgetClass *parent_class = NULL;

guint gtk_slider_get_type(void) {
  static guint slider_type = 0;

  if (!slider_type) {
    GtkTypeInfo slider_info = {
      "Gtkslider",
      sizeof (Gtkslider),
      sizeof (GtksliderClass),
      (GtkClassInitFunc) gtk_slider_class_init,
      (GtkObjectInitFunc) gtk_slider_init,
      NULL,
      NULL,
    };

    slider_type = gtk_type_unique(gtk_widget_get_type(), &slider_info);
  }

  return slider_type;
}

static void gtk_slider_class_init (GtksliderClass *class) {
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;

  object_class = (GtkObjectClass*) class;
  widget_class = (GtkWidgetClass*) class;

  parent_class = gtk_type_class(gtk_widget_get_type());

  object_class->destroy = gtk_slider_destroy;

  widget_class->realize = gtk_slider_realize;
  widget_class->expose_event = gtk_slider_expose;
  widget_class->size_request = gtk_slider_size_request;
  widget_class->size_allocate = gtk_slider_size_allocate;
  widget_class->button_press_event = gtk_slider_button_press;
  widget_class->button_release_event = gtk_slider_button_release;
  widget_class->motion_notify_event = gtk_slider_motion_notify;
}

static void gtk_slider_init (Gtkslider *slider) {
  slider->policy = GTK_UPDATE_CONTINUOUS;
  slider->state = STATE_IDLE;
  slider->saved_x = slider->saved_y = 0;
  slider->timer = 0;
  slider->pixbuf = NULL;
  slider->old_value = 0.0;
  slider->old_lower = 0.0;
  slider->old_upper = 0.0;
  slider->adjustment = NULL;
}

GtkWidget *gtk_slider_new(GtkAdjustment *adjustment) {
  Gtkslider *slider;

  slider = gtk_type_new(gtk_slider_get_type());

  if (!adjustment)
    adjustment = (GtkAdjustment*) gtk_adjustment_new(0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

  gtk_slider_set_adjustment(slider, adjustment);

  return GTK_WIDGET(slider);
}

static void gtk_slider_destroy(GtkObject *object) {
  Gtkslider *slider;

  g_return_if_fail(object != NULL);
  g_return_if_fail(GTK_IS_slider(object));

  slider = GTK_slider(object);

  if (slider->adjustment) {
    gtk_object_unref(GTK_OBJECT(slider->adjustment));
    slider->adjustment = NULL;
  }

  if (slider->pixbuf) {
    gdk_pixbuf_unref(slider->pixbuf);
    slider->pixbuf = NULL;
  }

  if (GTK_OBJECT_CLASS(parent_class)->destroy)
    (*GTK_OBJECT_CLASS(parent_class)->destroy)(object);
}

GtkAdjustment* gtk_slider_get_adjustment(Gtkslider *slider) {
  g_return_val_if_fail(slider != NULL, NULL);
  g_return_val_if_fail(GTK_IS_slider(slider), NULL);

  return slider->adjustment;
}

void gtk_slider_set_update_policy(Gtkslider *slider, GtkUpdateType policy) {
  g_return_if_fail (slider != NULL);
  g_return_if_fail (GTK_IS_slider (slider));

  slider->policy = policy;
}

void gtk_slider_set_adjustment(Gtkslider *slider, GtkAdjustment *adjustment) {
  g_return_if_fail (slider != NULL);
  g_return_if_fail (GTK_IS_slider (slider));

  if (slider->adjustment) {
    gtk_signal_disconnect_by_data(GTK_OBJECT(slider->adjustment), (gpointer)slider);
    gtk_object_unref(GTK_OBJECT(slider->adjustment));
  }

  slider->adjustment = adjustment;
  gtk_object_ref(GTK_OBJECT(slider->adjustment));
  gtk_object_sink(GTK_OBJECT( slider->adjustment ) ); 

  gtk_signal_connect(GTK_OBJECT(adjustment), "changed",
		     GTK_SIGNAL_FUNC(gtk_slider_adjustment_changed), (gpointer) slider);
  gtk_signal_connect(GTK_OBJECT(adjustment), "value_changed",
		     GTK_SIGNAL_FUNC(gtk_slider_adjustment_value_changed), (gpointer) slider);

  slider->old_value = adjustment->value;
  slider->old_lower = adjustment->lower;
  slider->old_upper = adjustment->upper;

  gtk_slider_update(slider);
}

static void gtk_slider_realize(GtkWidget *widget) {
  Gtkslider *slider;
  GdkWindowAttr attributes;
  gint attributes_mask;

  g_return_if_fail(widget != NULL);
  g_return_if_fail(GTK_IS_slider(widget));

  GTK_WIDGET_SET_FLAGS(widget, GTK_REALIZED);
  slider = GTK_slider(widget);

  attributes.x = widget->allocation.x;
  attributes.y = widget->allocation.y;
  attributes.width = widget->allocation.width;
  attributes.height = widget->allocation.height;
  attributes.wclass = GDK_INPUT_OUTPUT;
  attributes.window_type = GDK_WINDOW_CHILD;
  attributes.event_mask =
    gtk_widget_get_events (widget) | 
    GDK_EXPOSURE_MASK | GDK_BUTTON_PRESS_MASK | 
    GDK_BUTTON_RELEASE_MASK | GDK_POINTER_MOTION_MASK |
    GDK_POINTER_MOTION_HINT_MASK;
  attributes.visual = gtk_widget_get_visual(widget);
  attributes.colormap = gtk_widget_get_colormap(widget);

  attributes_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;
  widget->window = gdk_window_new(widget->parent->window, &attributes, attributes_mask);

  widget->style = gtk_style_attach(widget->parent->style, widget->window);

  gdk_window_set_user_data(widget->window, widget);

  slider->pixbuf = gdk_pixbuf_new_from_file(INSTALL_DIR"/slider.png",&gerror);

  gtk_style_set_background(widget->style, widget->window, GTK_STATE_NORMAL);

  // this is all it takes to make the widget take on the parent's background image
  gdk_window_set_back_pixmap(widget->window, widget->parent->style->bg_pixmap[GTK_STATE_NORMAL],GDK_PARENT_RELATIVE);

}

static void gtk_slider_size_request (GtkWidget *widget, GtkRequisition *requisition) {
  requisition->width = 26;
  requisition->height = 50;	// FIXME - this shouldn't be hardcoded
}

static void gtk_slider_size_allocate (GtkWidget *widget, GtkAllocation *allocation) {
  Gtkslider *slider;

  g_return_if_fail(widget != NULL);
  g_return_if_fail(GTK_IS_slider(widget));
  g_return_if_fail(allocation != NULL);

  widget->allocation = *allocation;
  slider = GTK_slider(widget);

  if (GTK_WIDGET_REALIZED(widget)) {
    gdk_window_move_resize(widget->window,
			   allocation->x, allocation->y,
			   allocation->width, allocation->height);
  }
}

static gint gtk_slider_expose(GtkWidget *widget, GdkEventExpose *event) {
  Gtkslider *slider;
  gfloat dx, dy;

  g_return_val_if_fail(widget != NULL, FALSE);
  g_return_val_if_fail(GTK_IS_slider(widget), FALSE);
  g_return_val_if_fail(event != NULL, FALSE);

  if (event->count > 0)
    return FALSE;
        
  slider = GTK_slider(widget);

  // FIXME - somewhere in here, we need to read this from the slider size

  // basically we need to work out if the step size is integer
  // if it is, centre the slider about the vertical
  
  dx = slider->adjustment->value - slider->adjustment->lower;	// value, from 0
  dy = slider->adjustment->upper - slider->adjustment->lower;	// range

  
  if (slider->adjustment->step_increment != 1.0f) {
	  dx=(int)(36*dx/dy)*50;
  } else {
	  dx=(int)(dx*12);
  }
  
  
  gdk_draw_pixbuf(widget->window, NULL, slider->pixbuf,
                  0, 0, 0, 0, 26, SLIDER_SIZE-10, GDK_RGB_DITHER_NONE, 0, 0);
  gdk_draw_pixbuf(widget->window, NULL, slider->pixbuf,
                  0, 78, 0, SLIDER_SIZE-10, 26, 8, GDK_RGB_DITHER_NONE, 0, 0);

// FIXME - just draw it for now  
  gdk_draw_pixbuf(widget->window, NULL, slider->pixbuf,
                  0, 89, 0, 10+dx, 26, 19, GDK_RGB_DITHER_NONE, 0, 0);


  return FALSE;
}

static gint gtk_slider_button_press(GtkWidget *widget, GdkEventButton *event) {
  Gtkslider *slider;

  g_return_val_if_fail(widget != NULL, FALSE);
  g_return_val_if_fail(GTK_IS_slider(widget), FALSE);
  g_return_val_if_fail(event != NULL, FALSE);

  slider = GTK_slider(widget);

  switch (slider->state) {
    case STATE_IDLE:
      switch (event->button) {
	case 1:
	case 3:
	  gtk_grab_add(widget);
	  slider->state = STATE_PRESSED;
	  slider->saved_x = event->x;
	  slider->saved_y = event->y;
	  break;

	default:
	  break;
      }
      break;

    default:
      break;
  }

  return FALSE;
}

static gint gtk_slider_button_release(GtkWidget *widget, GdkEventButton *event) {
  Gtkslider *slider;

  g_return_val_if_fail(widget != NULL, FALSE);
  g_return_val_if_fail(GTK_IS_slider(widget), FALSE);
  g_return_val_if_fail(event != NULL, FALSE);

  slider = GTK_slider(widget);

  switch (slider->state) {
    case STATE_PRESSED:
      gtk_grab_remove(widget);
      slider->state = STATE_IDLE;

      switch (event->button) {
	case 1:
	  slider->adjustment->value -= slider->adjustment->page_increment;
	  gtk_signal_emit_by_name(GTK_OBJECT(slider->adjustment), "value_changed");
	  break;

	case 3:
	  slider->adjustment->value += slider->adjustment->page_increment;
	  gtk_signal_emit_by_name(GTK_OBJECT(slider->adjustment), "value_changed");
	  break;

	default:
	  break;
      }
      break;

    case STATE_DRAGGING:
      gtk_grab_remove(widget);
      slider->state = STATE_IDLE;

      if (slider->policy != GTK_UPDATE_CONTINUOUS && slider->old_value != slider->adjustment->value)
	gtk_signal_emit_by_name(GTK_OBJECT(slider->adjustment), "value_changed");

      break;

    default:
      break;
  }

  return FALSE;
}

static gint gtk_slider_motion_notify(GtkWidget *widget, GdkEventMotion *event) {
  Gtkslider *slider;
  GdkModifierType mods;
  gint x, y;

  g_return_val_if_fail(widget != NULL, FALSE);
  g_return_val_if_fail(GTK_IS_slider(widget), FALSE);
  g_return_val_if_fail(event != NULL, FALSE);

  slider = GTK_slider(widget);

  x = event->x;
  y = event->y;

  if (event->is_hint || (event->window != widget->window))
    gdk_window_get_pointer(widget->window, &x, &y, &mods);

  switch (slider->state) {
    case STATE_PRESSED:
      slider->state = STATE_DRAGGING;
      /* fall through */

    case STATE_DRAGGING:
      if (mods & GDK_BUTTON1_MASK) {
	gtk_slider_update_mouse(slider, x, y, TRUE);
	return TRUE;
      } else if (mods & GDK_BUTTON3_MASK) {
	gtk_slider_update_mouse(slider, x, y, FALSE);
	return TRUE;
      }
      break;

    default:
      break;
  }

  return FALSE;
}

static gint gtk_slider_timer(Gtkslider *slider) {
  g_return_val_if_fail(slider != NULL, FALSE);
  g_return_val_if_fail(GTK_IS_slider(slider), FALSE);

  if (slider->policy == GTK_UPDATE_DELAYED)
    gtk_signal_emit_by_name(GTK_OBJECT(slider->adjustment), "value_changed");

  return FALSE;	/* don't keep running this timer */
}

static void gtk_slider_update_mouse_update(Gtkslider *slider) {
  if (slider->policy == GTK_UPDATE_CONTINUOUS)
    gtk_signal_emit_by_name(GTK_OBJECT(slider->adjustment), "value_changed");
  else {
    gtk_widget_draw(GTK_WIDGET(slider), NULL);

    if (slider->policy == GTK_UPDATE_DELAYED) {
      if (slider->timer)
	gtk_timeout_remove(slider->timer);

      slider->timer = gtk_timeout_add (SCROLL_DELAY_LENGTH, (GtkFunction) gtk_slider_timer,
				     (gpointer) slider);
    }
  }
}

static void gtk_slider_update_mouse(Gtkslider *slider, gint x, gint y,
                                  gboolean absolute)
{
    gfloat old_value, new_value, dv, dh;
    gdouble angle;

    g_return_if_fail(slider != NULL);
    g_return_if_fail(GTK_IS_slider(slider));

    old_value = slider->adjustment->value;

// FIXME - this needs to be changed for a horizontal slider
    angle = atan2(-x + (SLIDER_SIZE>>1), y - (SLIDER_SIZE>>1));

    if (absolute) {	// leftclick

        angle /= M_PI;
        if (angle < -0.5)
            angle += 2;

        new_value = -(2.0/3.0) * (angle - 1.25);   /* map [1.25pi, -0.25pi] onto [0, 1] */
        new_value *= slider->adjustment->upper - slider->adjustment->lower;
        new_value += slider->adjustment->lower;

    } else { // rightclick

        dv = slider->saved_y - y; /* inverted cartesian graphics coordinate system */
        dh = x - slider->saved_x;
        slider->saved_x = x;
        slider->saved_y = y;

        if (x >= 0 && x <= SLIDER_SIZE)
            dh = 0;  /* dead zone */
        else {
            angle = cos(angle);
            dh *= angle * angle;
        }

        new_value = slider->adjustment->value +
                    dv * slider->adjustment->step_increment +
                    dh * (slider->adjustment->upper -
                          slider->adjustment->lower) / 200.0f;
    }

    new_value = MAX(MIN(new_value, slider->adjustment->upper),
                    slider->adjustment->lower);

    slider->adjustment->value = new_value;

    if (slider->adjustment->value != old_value)
        gtk_slider_update_mouse_update(slider);
}

static void gtk_slider_update(Gtkslider *slider) {
  gfloat new_value;
        
  g_return_if_fail(slider != NULL);
  g_return_if_fail(GTK_IS_slider (slider));

	// gjcp
	
  new_value = slider->adjustment->value;
  if (slider->adjustment->step_increment == 1) new_value = (int)(slider->adjustment->value+0.5);
        
  if (new_value < slider->adjustment->lower)
    new_value = slider->adjustment->lower;

  if (new_value > slider->adjustment->upper)
    new_value = slider->adjustment->upper;

  if (new_value != slider->adjustment->value) {
    slider->adjustment->value = new_value;
    gtk_signal_emit_by_name(GTK_OBJECT(slider->adjustment), "value_changed");
  }

  gtk_widget_draw(GTK_WIDGET(slider), NULL);
}

static void gtk_slider_adjustment_changed(GtkAdjustment *adjustment, gpointer data) {
  Gtkslider *slider;

  g_return_if_fail(adjustment != NULL);
  g_return_if_fail(data != NULL);

  slider = GTK_slider(data);

  if ((slider->old_value != adjustment->value) ||
      (slider->old_lower != adjustment->lower) ||
      (slider->old_upper != adjustment->upper)) {
    gtk_slider_update (slider);

    slider->old_value = adjustment->value;
    slider->old_lower = adjustment->lower;
    slider->old_upper = adjustment->upper;
  }
}

static void gtk_slider_adjustment_value_changed (GtkAdjustment *adjustment, gpointer data) {
  Gtkslider *slider;

  g_return_if_fail(adjustment != NULL);
  g_return_if_fail(data != NULL);

  slider = GTK_slider(data);

  if (slider->old_value != adjustment->value) {
    gtk_slider_update (slider);

    slider->old_value = adjustment->value;
  }
}
