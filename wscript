#! /usr/bin/env python
# encoding: utf-8

import os

# the following two variables are used by the target "waf dist"
VERSION='0.2'
APPNAME='nekobee'

# these variables are mandatory ('/' are converted automatically)
srcdir = '.'
blddir = 'build'

def set_options(opt):
    opt.tool_options('compiler_cc')
    #opt.tool_options('lv2plugin', tooldir='.')

def configure(conf):
    conf.check_tool('compiler_cc')
    #conf.check_tool('lv2plugin', tooldir='.')

    conf.check_cfg(package='dssi', args='--cflags --libs')
    conf.check_cfg(package='lv2core', args='--cflags --libs')
    conf.check_cfg(package='liblo', args='--cflags --libs')
    conf.check_cfg(package='gtk+-2.0', args='--cflags --libs')

    conf.env['DSSI_DIR'] = os.path.normpath(os.path.join(conf.env['PREFIX'], 'lib', 'dssi'))
    conf.env['INSTALL_DIR'] = os.path.join(conf.env['DSSI_DIR'], 'nekobee')
    conf.env['LV2_DIR'] = os.path.normpath(os.path.join(conf.env['PREFIX'], 'lib', 'lv2', 'nekobee.lv2'))

    conf.define('INSTALL_DIR', conf.env['INSTALL_DIR'])
    conf.write_config_header('config.h')

# /usr/lib/dssi/nekobee.la
# /usr/lib/dssi/nekobee.so
# /usr/lib/dssi/nekobee/bg.png
# /usr/lib/dssi/nekobee/knob.png
# /usr/lib/dssi/nekobee/nekobee_gtk
# /usr/lib/dssi/nekobee/newknob.png
# /usr/lib/dssi/nekobee/slider.png
# /usr/lib/dssi/nekobee/switch.png
# /usr/lib/lv2/nekobee.lv2/bg.png
# /usr/lib/lv2/nekobee.lv2/lv2_ui_dssi.la
# /usr/lib/lv2/nekobee.lv2/lv2_ui_dssi.so
# /usr/lib/lv2/nekobee.lv2/manifest.ttl
# /usr/lib/lv2/nekobee.lv2/nekobee.so
# /usr/lib/lv2/nekobee.lv2/nekobee.ttl
# /usr/lib/lv2/nekobee.lv2/nekobee_gtk
# /usr/lib/lv2/nekobee.lv2/newknob.png
# /usr/lib/lv2/nekobee.lv2/slider.png


def build(bld):
    # DSSI plugin
    plugin_dssi = bld.new_task_gen('cc', 'shlib')
    plugin_dssi.env['shlib_PATTERN'] = '%s.so'
    plugin_dssi.env.append_value("LINKFLAGS", "-module -avoid-version -Wc,-nostartfiles")
    plugin_dssi.includes = ['.', 'src']
    plugin_dssi.defines = 'HAVE_CONFIG_H'
    plugin_dssi.source = [
	'src/nekobee-dssi.c',
	'src/nekobee_data.c',
	'src/nekobee_ports.c',
	'src/nekobee_synth.c',
	'src/nekobee_voice.c',
	'src/nekobee_voice_render.c',
	'src/minblep_tables.c',
        ]
    plugin_dssi.target = 'nekobee'
    plugin_dssi.install_path = '${DSSI_DIR}/'
    bld.install_files('${INSTALL_DIR}', 'extra/*')

    # DSSI UI executable
    gui_gtk = bld.new_task_gen('cc', 'program')
    gui_gtk.includes = ['.', 'src', 'src/gtk']
    gui_gtk.defines = 'HAVE_CONFIG_H'
    gui_gtk.source = [
	'src/gtk/callbacks.c',
	'src/gtk/data.c',
	'src/gtk/interface.c',
	'src/gtk/knob.c',
	'src/gtk/slider.c',
	'src/gtk/main.c',
	'src/nekobee_data.c',
	'src/nekobee_ports.c',
        ]
    gui_gtk.uselib = 'GTK+-2.0 LIBLO'
    gui_gtk.target = 'nekobee_gtk'
    gui_gtk.install_path = '${INSTALL_DIR}/'

    # LV2 plugin
    plugin_lv2 = bld.new_task_gen('cc', 'shlib')
    plugin_lv2.env['shlib_PATTERN'] = '%s.so'
    plugin_lv2.env.append_value("LINKFLAGS", "-module -avoid-version -Wc,-nostartfiles")
    plugin_lv2.includes = ['.', 'src']
    plugin_lv2.defines = ['HAVE_CONFIG_H', 'USE_LV2']
    plugin_lv2.source = [
	'src/nekobee-dssi.c',
	'src/nekobee_data.c',
	'src/nekobee_ports.c',
	'src/nekobee_synth.c',
	'src/nekobee_voice.c',
	'src/nekobee_voice_render.c',
	'src/minblep_tables.c',
        ]
    plugin_lv2.target = 'nekobee'
    plugin_lv2.install_path = '${LV2_DIR}/'
    bld.install_files('${LV2_DIR}', 'src/*.ttl')
    bld.install_files('${LV2_DIR}', 'extra/*')

    # LV2 UI wrapper library
    lv2_ui_dssi = bld.new_task_gen('cc', 'shlib')
    lv2_ui_dssi.env['shlib_PATTERN'] = '%s.so'
    #lv2_ui_dssi.env.append_value("LINKFLAGS", "-module -avoid-version -Wc,-nostartfiles")
    lv2_ui_dssi.includes = ['.', 'src']
    lv2_ui_dssi.defines = ['HAVE_CONFIG_H', "UI_EXECUTABLE='\"nekobee_gtk\"'", "UI_URI='\"http://nekosynth.co.uk/wiki/nekobee-dssi-gui\"'"]
    lv2_ui_dssi.source = [
	'src/lv2_ui_dssi.c',
        ]
    lv2_ui_dssi.uselib = 'LIBLO'
    lv2_ui_dssi.target = 'lv2_ui_dssi'
    lv2_ui_dssi.install_path = '${LV2_DIR}/'

    # LV2 UI executable
    gui_gtk = bld.new_task_gen('cc', 'program')
    gui_gtk.includes = ['.', 'src', 'src/gtk']
    gui_gtk.defines = 'HAVE_CONFIG_H'
    gui_gtk.source = [
	'src/gtk/callbacks.c',
	'src/gtk/data.c',
	'src/gtk/interface.c',
	'src/gtk/knob.c',
	'src/gtk/slider.c',
	'src/gtk/main.c',
	'src/nekobee_data.c',
	'src/nekobee_ports.c',
        ]
    gui_gtk.uselib = 'GTK+-2.0 LIBLO'
    gui_gtk.target = 'nekobee_gtk'
    gui_gtk.install_path = '${LV2_DIR}/'
